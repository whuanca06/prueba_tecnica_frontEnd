# Portal de Noticias con Next.js

Este proyecto es un portal de noticias desarrollado con Next.js. Proporciona una plataforma simple para ver y crear noticias.

## Características

- **Página de Inicio:** Muestra las noticias más recientes.
- **Creación de Noticias:** Permite a los usuarios crear nuevas noticias.
- **Detalles de Noticias:** Visualización detallada de cada noticia.

## Tecnologías Utilizadas

- **Next.js:** Framework de React para aplicaciones web.
- **React:** Biblioteca de JavaScript para construir interfaces de usuario.
- **Material-UI:** Componentes de interfaz de usuario basados en Material Design.
- 
