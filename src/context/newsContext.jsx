"use client"

import { createContext, useContext, useEffect, useState } from "react";

export const newsContext = createContext();

export const useNews = () => {
    const context = useContext(newsContext);

    if (!context) throw new Error('useNews must used within a provider')

    return context;
}

export const NewsProvider = ({ children }) => {
    const [news, setNews] = useState([]);
    const [loading, setLoading] = useState(true);
  
    useEffect(() => {
      const fetchNews = async () => {
        try {
          const response = await fetch('http://localhost:4000/user-news');
          const data = await response.json();
          
          setNews(data);
        } catch (error) {
          console.error('Error fetching news:', error);
        } finally {
          setLoading(false);
        }
      };
  
      fetchNews();
    }, []); 
  
    return (
      <newsContext.Provider value={{ news, loading }}>
        {children}
      </newsContext.Provider>
    );
  };