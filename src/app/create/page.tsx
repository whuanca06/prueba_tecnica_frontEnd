"use client"
import React, { ChangeEvent, useState } from 'react';
import { Box, FormControl, InputLabel, Input, Stack, Typography, Button } from '@mui/material';
import axios from 'axios';

const MyForm = () => {

  const [task, setTask] = useState<Object>({});
  const handlechange = (event: ChangeEvent<HTMLInputElement>) => {

    setTask({ ...task, [event.target.name]: event.target.value });
    
    
  };
  const handleSubmit = async (event:any) => {
    event.preventDefault();
    try {
      // Cambia la URL con la dirección de tu API REST
      const response = await axios.post('http://localhost:4000/user-news', task);
      console.log('Respuesta del servidor:', response.data);
    } catch (error) {
      console.error('Error al enviar la solicitud PUT:', error);
    }
    
    
  };

  

  return (
    <Box
      component="form"
      onSubmit={handleSubmit} 
      sx={{
        border: '1px solid #ccc',
        borderRadius: '8px',
        padding: '16px',
        '& > :not(style)': { m: 1 },
      }}
      noValidate
      autoComplete="off"
    >
      <Stack spacing={2}>
        <Typography variant="h6">Crear una nueva noticia</Typography>

        <FormControl variant="standard">
          <InputLabel htmlFor="component-simple">Titulo de la noticia</InputLabel>
          <Input id="title" defaultValue="Titulo" 
          name='title'
          onChange={handlechange} />
        </FormControl>

        <FormControl variant="standard">
          <InputLabel htmlFor="component-helper">Contenido de la noticia</InputLabel>
          <Input
            id="content"
            name="content"
            defaultValue="contenido"
            aria-describedby="component-helper-text"
            onChange={handlechange}
          />
        </FormControl>

        <FormControl variant="standard">
          <InputLabel htmlFor="component-helper">Lugar</InputLabel>
          <Input
            id="place"
            name="place"
            defaultValue="Lugar del cometido"
            aria-describedby="component-helper-text"
            onChange={handlechange}
          />
        </FormControl>

        <FormControl variant="standard">
          <InputLabel htmlFor="component-helper">Autor</InputLabel>
          <Input
            id="author"
            name="author"
            defaultValue="Autor"
            aria-describedby="component-helper-text"
            onChange={handlechange}
          />
        </FormControl>


        <Button type="submit" variant="contained" color="primary">
          Guardar
        </Button>
      </Stack>
    </Box>
  );
}

export default MyForm;
