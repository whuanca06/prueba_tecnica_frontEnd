
"use client"
import { Button, Card, CardActions, CardContent, Grid, Typography, darken, styled } from '@mui/material';
import { useNews } from '../context/newsContext'
import { useRouter } from 'next/navigation';

export default function Home() {
  const router = useRouter();
  const { news, loading } = useNews();

  const StyledButton = styled(Button)({
    border: '2px solid', // Grosor del borde
    borderRadius: '8px', // Radio de la esquina para un aspecto más redondeado
    padding: '8px 16px', // Ajuste el padding para hacer el botón más grande
    ':hover': {
      color: darken('#6e1919', 0.2),
    },
  });

  return (
    
    <div>
      <StyledButton
      size="medium" // Ajusta el tamaño según tus preferencias
      onClick={() => router.push('/create')}
    >
      Crear Noticia
    </StyledButton>
      {loading ? (
        <p>Cargando...</p>
      ) : (
        <ul>

          <Grid container spacing={2}>
            {news.map((item: any) => (
              <Grid item key={item.id} xs={12} sm={6} md={4} lg={3}>
                <Card
                  sx={{ maxWidth: 175 }}
                >
                  <CardContent>
                    <Typography sx={{ fontSize: 14 }} color="text.secondary" gutterBottom>
                      {item.author}
                    </Typography>
                    <Typography variant="h5" component="div">
                      {item.title}
                    </Typography>
                    <Typography sx={{ fontSize: 12 }} color="text.secondary">
                      {new Date(item.publicationDate).toLocaleString()}
                    </Typography>
                    <Typography variant="body2">
                      {item.content}
                    </Typography>
                  </CardContent>
                  <CardActions>
                    <Button
                      size="small"
                      sx={{ color: 'red', ':hover': { color: darken('#6e1919', 0.2) } }}
                    >Eliminar</Button>
                    <Button
                      size="small"
                      sx={{ color: 'back', ':hover': { color: darken('#6e1919', 0.2) } }}
                      onClick={() => router.push(`/edit/${item.id}`)}
                    >Editar</Button>
                  </CardActions>
                </Card>
              </Grid>
            ))}
          </Grid>
        </ul>
      )}
    </div>
  );
}