import type { Metadata } from 'next'
import { Inter } from 'next/font/google'
import './globals.css'
import { NewsProvider } from '../context/newsContext'
const inter = Inter({ subsets: ['latin'] })

export const metadata: Metadata = {
  title: 'CRUD HOCICON',
  description: 'hocicon portal de noticias',
}

export default function RootLayout({
  children,
}: {
  children: React.ReactNode
}) {
  return (
    <html lang="en">
      <body className={inter.className}>
        <NewsProvider>
          {children}
        </NewsProvider>
      </body>
    </html>
  )
}
