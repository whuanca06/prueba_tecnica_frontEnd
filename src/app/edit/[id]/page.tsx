import React, { FC } from 'react';

interface PageProps {
  params: {
    id: string;
  };
}

const PageEdit: FC<PageProps> = ({ params }) => {
  return (
    <div>page de edit {params.id}</div>
  );
}

export default PageEdit;